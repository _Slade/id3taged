typedef struct {
    char tag     [ 3];
    char title   [30];
    char artist  [30];
    char album   [30];
    char year    [ 4];
    char comment [28];
    unsigned char nullsep;
    unsigned char track;
    unsigned char genre;
} ID3v1;

struct options {
    bool getopt_success;
    bool display_only;
    char const *filename;
    char const *title;
    char const *artist;
    char const *album;
    char const *comment;
    char const *year;
    char const *track;
    char const *genre;
};

static char  *get_genre(int);
static void   getopt(struct options *, int, char **);
static bool   getopt_year_is_valid(char const *);
static void   merge_tag_opts(ID3v1 *, struct options *);
static void   print_tag(ID3v1 *, FILE *);
static ID3v1 *read_tag(FILE *);
static size_t write_tag(ID3v1 *, FILE *);

#ifdef FILECHECK
    static bool   is_known_fmt(FILE *);
#endif
