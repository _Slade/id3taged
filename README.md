# NAME #
id3tagEd
# USAGE #
    id3tagEd *filename* [-*field* *value*]

`field`, if supplied, must be one of: title, artist, album, year,
comment, track and *value* is its replacement. Multiple `field`
parameters can be specified. A -`field` parameter must be followed by a
`value` parameter. If redundant field specifications are given, the last
one will be used.

# LIMITATIONS #
* Only ID3v1.1 is supported.
* Value fields cannot begin 
* If a new tag is created and no genre is specified, it will default to 0 ("Blues"). This is a fairly common behavior for ID3 tag editors.
* Genres will be displayed as their string representations, but must be provided as numbers when setting them.
* This program assumes "CHAR_BIT == 8".