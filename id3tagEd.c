/*
=head1 NAME

id3tagEd

=head1 USAGE

id3tagEd I<filename> [-I<field> I<value>]

I<field>, if supplied, must be one of:
    title, artist, album, year, comment, track
and I<value> is its replacement. Multiple I<field> parameters can be specified.
A -I<field> parameter must be followed by a I<value> parameter.
If redundant field specifications are given, the last one will be used.

=head1 LIMITATIONS

Value fields cannot begin with '-'.

If a new tag is created and no genre is specified, it will default to 0
("Blues"). This is a fairly common behavior for ID3 tag editors.

Genres will be displayed as their string representations, but must be provided
as numbers when setting them.

This program assumes C<CHAR_BIT == 8>.

=cut
*/
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "id3tagEd.h"

#define LOGERR(msg) fputs(msg, stderr)
#define LOGERRF(msg, ...) fprintf(stderr, msg, __VA_ARGS__)

static char const * const usage =
    "Usage:\n"
    "\tid3tagEd FILENAME [-FIELD VALUE]\n"
    "Valid options for FIELD are:\n"
    "\ttitle\n"
    "\tartist\n"
    "\talbum\n"
    "\tyear\n"
    "\tcomment\n"
    "\ttrack\n"
    "\tgenre"
;

int main(int argc, char *argv[])
{
    struct options options = {0};
    getopt(&options, argc, argv);

    if (!options.getopt_success)
    {
        return EXIT_FAILURE;
    }

    if (options.display_only)
    {
        FILE *infile = fopen(options.filename, "rb");
        ID3v1 *tag   = read_tag(infile);
        if (infile == NULL)
        {
            LOGERRF(
                "Error: could not open file %s: %s.\n",
                options.filename,
                strerror(errno)
            );
            return EXIT_FAILURE;
        }
        fclose(infile);

        if (tag != NULL)
        {
            print_tag(tag, stdout);
            free(tag);
            return EXIT_SUCCESS;
        }
        return EXIT_FAILURE;
    }
    /* From here we're either writing or appending a new tag */
    FILE *outfile = fopen(options.filename, "r+b");
    if (outfile == NULL)
    {
        LOGERRF(
            "Error: Couldn't open file %s: %s.\n",
            options.filename,
            strerror(errno)
        );
        return EXIT_FAILURE;
    }
    ID3v1 *new_tag = read_tag(outfile);
    if (new_tag == NULL) /* File has no tag, create a new one */
    {
        new_tag = calloc(sizeof(ID3v1), sizeof(char));
        new_tag->tag[0] = 'T';
        new_tag->tag[1] = 'A';
        new_tag->tag[2] = 'G';
        merge_tag_opts(new_tag, &options);
        if(!freopen(options.filename, "ab", outfile)) /* Reopen for appending */
        {
            LOGERRF(
                "Error reading from file: %s; exiting.\n",
                strerror(errno)
            );
            if (outfile)
                fclose(outfile);
            return(EXIT_FAILURE);
        }
        fwrite(new_tag, 1, sizeof(ID3v1), outfile);
    }
    else
    {
        merge_tag_opts(new_tag, &options);
        if (write_tag(new_tag, outfile) == -1)
        {
            free(new_tag);
            fclose(outfile);
            return EXIT_FAILURE;
        }
    }
    /* Cleanup */
    free(new_tag);
    fclose(outfile);
    return EXIT_SUCCESS;
}
/**
=head1 FUNCTIONS

=head2 void merge_tag_opts(ID3v1 *tag, struct options *options)

Merge new field values from I<options>, overwriting those in I<tag>.

This function is somewhat redundant with getopt(), which could directly write
these fields as it encounters them when going over the command line options.
However, prior to calling getopt(), we do not know whether or not a new tag is
being created or an existing one from a file is being modified, so rather than
doing:
    Open file
    Look for tag
    Create new tag if necessary
    Parse options
    Fill fields
I opted for
    Parse options (get filename and fields)
    Open file
    Look for tag
    Create new tag if necessary
    Fill fields

=cut
*/
void merge_tag_opts(ID3v1 *tag, struct options *options)
{
    if (options->title != NULL)
        strncpy(tag->title,  options->title,    30UL);
    if (options->artist != NULL)
        strncpy(tag->artist, options->artist,   30UL);
    if (options->album != NULL)
        strncpy(tag->album,  options->album,    30UL);
    if (options->comment != NULL)
        strncpy(tag->comment, options->comment, 28UL);
    if (options->year != NULL)
        strncpy(tag->year,   options->year,      4UL);
    if (options->genre != NULL)
        tag->genre = (unsigned char)atoi(options->genre);
    if (options->track != NULL)
        tag->track = (unsigned char)atoi(options->track);
}

/**
=head2 void getopt(struct options *setopt, int argc, char **argv);

Parse command line parameters, set options as necessary.

=cut
*/
void getopt(struct options *setopt, int argc, char **argv)
{
    if (argc < 2 || argc & 1) /* No filename or odd number of parameters */
    {
        puts(usage);
        setopt->getopt_success = false;
        return;
    }

    setopt->filename     = argv[1];
    setopt->display_only = false;

    if (argc == 2) /* ./id3tagEd FILENAME or ./id3tagEd -help */
    {
        if (!strcmp(argv[1], "-help") || !strcmp(argv[1], "-h"))
        {
            puts(usage);
            exit(EXIT_SUCCESS);
        }
        setopt->display_only   = true;
        setopt->getopt_success = true;
        return;
    }

    for (int i = 2; i < argc - 1; i += 2)
    {
        char const *newval = argv[i + 1];
        if (newval == NULL)
        {
            LOGERR("Missing value for field.\n");
            setopt->getopt_success = false;
            return;
        }

        if (!strcmp(argv[i], "-title"))
            setopt->title = newval;
        else if (!strcmp(argv[i], "-artist"))
            setopt->artist = newval;
        else if (!strcmp(argv[i], "-album"))
            setopt->album = newval;
        else if (!strcmp(argv[i], "-comment"))
            setopt->comment = newval;
        else if (!strcmp(argv[i], "-year"))
        {
            if (getopt_year_is_valid(newval))
            {
                setopt->year = newval;
            }
            else
            {
                LOGERR("Invalid year given, must be all digits.\n");
                setopt->getopt_success = false;
                return;
            }
        }
        else if (!strcmp(argv[i], "-track"))
            setopt->track = newval;
        else if (!strcmp(argv[i], "-genre"))
            setopt->genre = newval;
        else
            LOGERR("Invalid field specified; skipping.\n");
    }
    setopt->getopt_success = true;
}

bool getopt_year_is_valid(char const *year)
{
    bool valid = true;
    if (strlen(year) != 4) /* Fail for inputs that are too short */
        return false;
    for (int i = 0; i < 4; ++i)
        valid &= ('0' <= year[i] && year[i] <= '9');
    return valid;
}

/**
=head2 ID3v1 *read_tag(FILE *fh)

Read an ID3 v1.1 tag from I<fh>, if possible.

Returns NULL if the file does not have an ID3v1.1 tag to be read.
Otherwise, returns a newly allocated ID3v1 tag with the respective fields
matching those in the file.

=cut
*/
ID3v1 *read_tag(FILE *fh)
{
    if (fh == NULL)
    {
        LOGERR("NULL filehandle passed to read_tag()\n");
        exit(EXIT_FAILURE);
    }
    if (fseek(fh, -128L, SEEK_END) == -1)
    {
        LOGERRF(
            "Error seeking to end of file: %s; exiting.\n",
            strerror(errno)
        );
        fclose(fh);
        exit(EXIT_FAILURE);
    }

    ID3v1 *tag = calloc(sizeof(ID3v1), sizeof(char));
    (void) fread(tag, 8ul, 128, fh);

    /* Check if what we've read looks like an ID3v1 tag */
    if (tag->tag[0] != 'T' || tag->tag[1] != 'A' || tag->tag[2] != 'G')
    {
        LOGERR("File does not have ID3v1.1 tag.\n");
        free(tag);
        return NULL;
    }

    return tag;
}

/**
 * Human-readable output of an ID3v1 tag to a specified stream
 */
void print_tag(ID3v1 *tag, FILE *stream)
{
    char *field_names[] = {
        "Title:", "Artist:", "Album:", "Year:", "Comment:"
    };
    int field_widths[]  = { 30, 30, 30, 4, 28 };
    unsigned char *tag_raw = ((unsigned char *)tag) + 3;
    for (int i = 0; i < 5; ++i)
    {
        printf("%-10s", field_names[i]);
        for (int j = 0; j < field_widths[i]; ++j)
        {
            unsigned char byte = *tag_raw++;
            if (byte != '\0')
                fputc(byte, stdout);
        }
        printf("\n");
    }

    printf("%-10s%d\n", "Track #:", tag->track);
    char *genre = get_genre(tag->genre);
    printf("%-10s%s\n", "Genre:", genre);
}

/**
=head2 char *get_genre(int genre_ID)

Return a readonly string representation of I<genre_ID>, for human
consumption. Winamp extensions are not supported.

=cut
*/
char *get_genre(int genre_ID)
{
    static char *genres[] = {
        "Blues",             "Classic Rock",   "Country",          "Dance",
        "Disco",             "Funk",           "Grunge",           "Hip-Hop",
        "Jazz",              "Metal",          "New Age",          "Oldies",
        "Other",             "Pop",            "Rhythm and Blues", "Rap",
        "Reggae",            "Rock",           "Techno",           "Industrial",
        "Alternative",       "Ska",            "Death Metal",      "Pranks",
        "Soundtrack",        "Euro-Techno",    "Ambient",          "Trip-Hop",
        "Vocal",             "Jazz & Funk",    "Fusion",           "Trance",
        "Classical",         "Instrumental",   "Acid",             "House",
        "Game",              "Sound Clip",     "Gospel",           "Noise",
        "Alternative Rock",  "Bass",           "Soul",             "Punk rock",
        "Space",             "Meditative",     "Instrumental Pop", "Instrumental Rock",
        "Ethnic",            "Gothic",         "Darkwave",         "Techno-Industrial",
        "Electronic",        "Pop-Folk",       "Eurodance",        "Dream",
        "Southern Rock",     "Comedy",         "Cult",             "Gangsta",
        "Top 40",            "Christian Rap",  "Pop/Funk",         "Jungle",
        "Native American",   "Cabaret",        "New Wave",         "Psychedelic",
        "Rave",              "Showtunes",      "Trailer",          "Lo-Fi",
        "Tribal",            "Acid Punk",      "Acid Jazz",        "Polka",
        "Retro",             "Musical",        "Rock & Roll",      "Hard Rock"
    };

    if (genre_ID > 79 || genre_ID < 0)
    {
        LOGERR("Invalid genre specified");
        return "n/a";
    }

    return genres[genre_ID];
}

#ifdef FILECHECK
bool is_known_fmt(FILE *fp)
{
    char buf[14];
    char input[3];
    char mp3_sig[3]  = "ID3";
    char flac_sig[8] = "fLaC\0\0\0\"";
    char ogg_sig[14] = "OggS\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00";

    (void) fread(buf, 1, 14, fp);
    if (ferror(fp))
    {
        LOGERRF(
            "Error reading from file: %s; exiting.\n",
            strerror(errno)
        );
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    /* if no signatures match */
    if (memcmp(buf, mp3_sig,   3)
    &&  memcmp(buf, flac_sig,  8)
    &&  memcmp(buf, ogg_sig,  14))
    {
        printf(
            "File does not appear to be a recognized audio format."
            " Modify anyway? "
        );
        (void) fgets(input, 3, stdin);
        return buf[0] != 'y' && buf[1] != 'e' && buf[2] != 's';
    }
    return true;
}
#endif

/**
=head2 size_t write_tag(ID3v1 *tag, FILE *outfile)

Write I<tag> to the end of I<outfile>, clobbering an existing tag. DO NOT use
on a file that does not contain an ID3 tag, as this will corrupt the last
128 bytes. If compiled with -DFILECHECK, write_tag() will first determine if
I<outfile> matches the file signatures that mark it as an MP3, FLAC, or OGG
file and prompt before continuing.

Make sure I<outfile> is opened with B<r+> to avoid clobbering the file.

Returns the number of bytes written. If this is not 128, something went wrong
with the ID3 tag.

=cut
*/
size_t write_tag(ID3v1 *tag, FILE *outfile)
{
#ifdef FILECHECK
    if (!is_known_fmt(outfile))
        return -1;
#endif

    if (fseek(outfile, -128L, SEEK_END) == -1)
    {
        LOGERRF(
            "Error seeking to end of file: %s; exiting.\n",
            strerror(errno)
        );
        return -1;
    }

    return fwrite(tag, 1, sizeof(ID3v1), outfile);
}

